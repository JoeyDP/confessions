import poll
import atexit

people = poll.People()
people.load()
atexit.register(people.save)

while True:
    print("These people were found:")
    for person in people.getPeople():
        print("\t", person)

    print("Add aliasses by typing two names (separated by comma)")
    print("Remove aliasses by typing two names that are already connected (separated by comma)")
    print("remove people by typing their name")
    print("add people by typing their name")
    print("exit by typing exit, quit or stop")
    inp = input()

    words = inp.split(",")
    if words[0] == "quit" or words[0] == "stop" or words[0] == "exit":
        break
    elif len(words) == 1:
        name = words[0].strip().lower()

        person = people.getPerson(name)
        if person is None:
            person = people.addPerson(name)
        else:
            people.removePerson(name)

    elif len(words) == 2:
        name1 = words[0].strip().lower()
        name2 = words[1].strip().lower()

        person1 = people.getPerson(name1)
        if person1 is None:
            person1 = people.addPerson(name1)

        if name2 in person1.names:
            people.removeAlias(name2)
            people.addPerson(name2)
        else:
            people.addAlias(person1, name2)
            people.removePerson(name2)



    else:
        print("Incorrect syntax!")
        print()


