import gspread
import sys
import operator
import pickle
import atexit
import os
import getpass

class Poll:

    def __init__(self):

        print("Welcome to the poll counter")
        print("This program will count people's names in a poll")
        print("After the program has ran, you can edit the Aliasses.txt file to add aliasses.")
        print("Manual: Simply write all the names of a person on the same line, separated by commas")
        print("e.g.: Coockie, Michiel")
        print("Fancy: run aliasses.py")
	print("The results get stored in results.txt")
        print()

        if os.path.exists("settings.dat"):
            with open("settings.dat", "rb") as file:
                data = pickle.Unpickler(file).load()
                self.userName = data[0]
                self.password = data[1]
        else:
            self.userName = ""
            self.password = ""

        client = self.getClient()
        sheet = self.getSheet(client)
        startColumn = self.getColumn()
        table = list()
        self.people = People()
        self.people.load()

        atexit.register(self.people.save)
        atexit.register(self.save)

        i = 1
        while True:
            line = sheet.row_values(i)
            if len(line) == 0 or line[0] is None:
                break
            table.append(line[startColumn:])
            i += 1

        header = table[0]
        table = table[1:]



        self.calculate(table)

        print("=========== Per quality ===========")
        index = 0
        for quality in self.people.perQuality():
            print(header[index])
            for vote, person in quality:
                print("Votes:", vote, "\t", person)
            index  += 1
            print()

        print("=========== Per person ===========")
        for person in self.people.getPeople():
            print(person)
            index = 0
            qualities = list()
            for vote in person.votes:
                qualities.append((vote, header[index]))
                index += 1

            qualities.sort(key=operator.itemgetter(0), reverse=True)
            for vote, quality in qualities:
                if vote > 0:
                    print(quality, "\t", vote)
            print()


    def save(self):
        with open("settings.dat", "wb") as file:
            data = (self.userName, self.password)
            pickle.Pickler(file).dump(data)


    def getIntInput(self, msg="", max=sys.maxsize, min=-sys.maxsize-1):
        """ Loops until it gets valid input. Prints a space after msg """
        try:
            inp = input(msg + " ")
            ret = int(inp)
        except ValueError:
            print(inp, "is not an integer, try again")
            return self.getIntInput(msg, max, min)
        if ret <= max and ret >= min:
            return ret
        else:
            print(ret, "is not a valid option")
            return self.getIntInput(msg, max, min)


    def getClient(self):
        try:
            if self.userName is "" or self.password is "":
                print("Please log in with you google drive account:")
                self.userName = input("e-mail: ")
                self.password = getpass.getpass("Password: ")
            client = gspread.login(self.userName, self.password)
        except gspread.AuthenticationError:
            print("Incorrect credentials!")
            self.userName = ""
            self.password = ""
            return self.getClient()
        return client


    def getSheet(self, client):
        try:
            print("What is the path to the google spreadsheet?")
            name = input()
            sheet = client.open(name).get_worksheet(0)
            return sheet
        except gspread.SpreadsheetNotFound:
            print("File not found, try again.")
            return self.getSheet(client)


    def getColumn(self):
        print("At what column do you want to start? (number from 0(A) to ...)")
        return self.getIntInput(min=1)


    def calculate(self, table):
        for row in table:
            column = 0
            for name in row:
                if name is None:
                    continue

                name.strip()
                person = self.people.getPerson(name)
                if person is None:
                    # print("Adding new person:", name)
                    # print("Add alias to someone?")
                    # for person in self.people.getPeople():
                    #    print(person)

                    # inp = input()
                    # if inp is "":
                    person = self.people.addPerson(name)
                    # else:
                    #     person = self.people.getPerson(inp)
                    #     if person is None:
                    #         print("Person not found")
                    #         person = self.people.addPerson(name)
                    #     else:
                    #         self.people.addAlias(person, name)

                person.addVote(column)
                column += 1


class Person():

    def __init__(self):
        self.votes = list()
        self.names = set()


    def addVote(self, index):
        while len(self.votes) <= index:
            self.votes.append(0)

        self.votes[index] += 1


    def addName(self, name):
        self.names.add(name)


    def __str__(self):
        first = True
        ret = ""
        for name in self.names:
            if first:
                ret = name
                first = False
            else:
                ret += ", " + name
            if len(self.names) == 1:
                return ret

        return ret


    def __repr__(self):
        return str(self)


class People():

    def __init__(self):
        self.people = dict();


    def save(self):
        with open("Aliasses.txt", "w") as file:
            for person in self.getPeople():
                file.write(str(person) + "\n")


    def load(self):
        if not os.path.exists("Aliasses.txt"):
            return

        with open("Aliasses.txt", "r") as file:
            lines = file.readlines()
            for line in lines:
                if line is "\n":
                    continue
                names = line.split(",")
                person = self.addPerson(names[0].strip().lower())
                for name in names[1:]:
                    self.addAlias(person, name.strip().lower())



    def addAlias(self, person, name):
        person.addName(name)
        self.people[name] = person


    def removeAlias(self, name):
        person = self.getPerson(name)
        person.names.remove(name)
        self.removePerson(name)

    def addPerson(self, name):
        person = Person()
        person.addName(name)
        self.people[name] = person
        return person

    def removePerson(self, name):
        del self.people[name]


    def getPerson(self, name):
        person = self.people.get(name, None)
        return person


    def getPeople(self):
        temp = set()
        for name, person in self.people.items():
            if person not in temp:
                temp.add(person)
                yield person


    def perQuality(self):
        qualities = list()
        for person in self.getPeople():
            index = 0
            for vote in person.votes:
                if vote > 0:
                    while len(qualities) <= index:
                        qualities.append(list())

                    qualities[index].append((vote, person))

                index += 1

        for quality in qualities:
            quality.sort(key=operator.itemgetter(0), reverse=True)
        return qualities



if __name__ == "__main__":
    Poll()

