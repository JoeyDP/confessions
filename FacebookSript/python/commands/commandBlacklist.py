import commands.command as command

class CommandBlacklist(command.Command):

    def __init__(self, c):
        super().__init__(c, "bl", "blacklist", "blist", args=["[type: soft/hard/check]", "[add/del/show]", "[value]"])

    def __call__(self, args=None):
        if args is not None:
            try:
                type = args[0]
                op = args[1]
            except IndexError:
                print("Suply type, operation and/or value!")
                return
        else:
            print("Suply type, operation and/or value!")
            return

        if type == "soft":
            bl = self.c.blacklist.getSoftBl()
        elif type == "hard":
            bl = self.c.blacklist.getHardBl()
        elif type == "check":
            bl = self.c.blacklist.getCheckBl()
        else:
            print(type, "is not a valid type!")
            return

        if op == "add":
            try:
                value = args[2]
            except IndexError:
                print("Suply a value!")
                return
            bl.append(value)
            print("Added", value, "to the", type, "blacklist.")
        elif op == "del":
            try:
                value = args[2]
            except IndexError:
                print("Suply a value!")
                return
            try:
                bl.remove(value)
            except ValueError:
                print(value, "not found in", type, "blacklist")
                return
            print("Deleted", value, "from the", type, "blacklist.")
        elif op == "show":
            print("--- ", type, "blaklist ---")
            for el in bl:
                print(el)
        else:
            print(op, "is not a valid operation")
            return



    def __str__(self):
        return "Takes the given blacklist (soft/hard/check) and either adds value to it, deletes value from it or displays it."