import re
import commands.commandHelp as commandHelp
import commands.commandSave as commandSave
import commands.commandBlacklist as commandBlacklist
import commands.commandDailytop as commandDailytop
import commands.commandGet as commandGet
import commands.commandSet as commandSet
import commands.commandPost as commandPost
import commands.commandQuit as commandQuit

class CommandHandler():

    def __init__(self, c):
        self.c = c
        self.regex = r"^\s*(?P<command>[^\s]+)(?:\s+(?P<args>.+))?"

        self.help = commandHelp.CommandHelp(c)
        self.save = commandSave.CommandSave(c)
        self.quit = commandQuit.CommandQuit(c)
        self.top = commandDailytop.CommandDailytop(c)
        self.bl = commandBlacklist.CommandBlacklist(c)
        self.get = commandGet.CommandGet(c)
        self.set = commandSet.CommandSet(c)
        self.post = commandPost.CommandPost(c)

        self.commands = list()
        self.commands.append(self.help)
        self.commands.append(self.save)
        self.commands.append(self.quit)
        self.commands.append(self.top)
        self.commands.append(self.bl)
        self.commands.append(self.get)
        self.commands.append(self.set)
        self.commands.append(self.post)


    def mainloop(self):

        # display help
        self.help()

        print("Program is in manual mode by default")

        ret = False

        while self.c.running:
            inp = input()

            # confirmation of confessions
            if inp == "":
                self.c.post = 1
                continue
            if inp == "no":
                self.c.post = 2
                continue

            match = re.search(self.regex, inp)
            match = match.groupdict()

            comm = match["command"]
            args = match["args"]
            if args is not None:
                args = args.split(" ")

            for command in self.commands:
                ret = command.check(comm, args)
                if ret:
                    break
            if not ret:
                print("Invalid syntax, type \"help\" for all commands")

            '''

            elif "manual" in inp:
                print("Disabled auto posting")
                self.post = 0
                confessions.c.autoPost = False
            elif inp.startswith("soft_bl_del"):
                word = inp[12:]
                try:
                    bl = self.get("softBL")
                    bl.remove(word)
                    self.set("softBL", bl)
                except ValueError:
                    print(word, "not found in soft blacklist.")
            elif inp.startswith("hard_bl_del"):
                word = inp[12:]
                try:
                    bl = self.get("hardBL")
                    bl.remove(word)
                    self.set("hardBL", bl)
                except ValueError:
                    print(word, "not found in hard blacklist.")
            elif inp.startswith("soft_bl"):
                word = inp[8:]
                list = self.get("softBL")
                list.append(word)
                self.set("softBL", list)
            elif inp.startswith("hard_bl"):
                word = inp[8:]
                list = self.get("hardBL")
                list.append(word)
                self.set("hardBL", list)
            elif inp.startswith("post_row"):
            elif "delay" in inp:
                if inp[6:] != "":
                    try:
                        self.delay = int(inp[6:])
                        print("Delay set to", self.delay, "seconds.")
                    except ValueError:
                        print("Incorrect syntax, try: \"row [what row to start checking at]\"")
            elif "row" in inp:
                if inp[4:] != "":
                    try:
                        self.set("currRow", int(inp[4:]))
                        print("Row set to", self.get("currRow"))
                    except ValueError:
                        print("Incorrect syntax, try: \"row [amount in seconds]\"")
            '''