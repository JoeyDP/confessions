import commands.command as command
import sys

class CommandQuit(command.Command):

    def __init__(self, c):
        super().__init__(c, "quit", "stop", "exit")

    def __call__(self, args=None):
        print("Quitting... (this may take a while)")
        self.c.running = False
        self.c.thread.join(5)
        sys.exit(0)

    def __str__(self):
        return "Saves all settings and quits."