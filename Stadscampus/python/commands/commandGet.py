import commands.command as command

class CommandGet(command.Command):

    def __init__(self, c):
        super().__init__(c, "get", args=["[The name of the setting]"])

    def __call__(self, args=None):
        if args is not None:
            name = args[0]
        else:
            print("Suply a name!")
            return

        if name in self.c.savedSettingNames:
            value = self.c.get(name)
        elif name in self.c.otherSettingsNames:
            if name == "autopost":
                value = self.c.autoPost
            elif name == "extendedOutput":
                value = self.c.extendedOutput
            elif name == "delay":
                value = self.c.delay
            elif name == "users":
                for user in self.c.users.users.items():
                    print(user)
                return
        else:
            print(name, "is not a settings!")
            return
        print(name, "is set to:", value)

    def __str__(self):
        return "Requests the value of one of the following settings:\n" + str(self.c.savedSettingNames + self.c.otherSettingsNames)