import commands.command as command
from top import Top

class CommandDailytop(command.Command):

    def __init__(self, c):
        super().__init__(c, "dailytop", "top", args=["[amount (optional)]"])
        self.top = list()

    def __call__(self, args=None):
        if args is not None:
            try:
                amount = int(args[0])
            except ValueError:
                print(args[0], "is not a valid integer")
                return
        else:
            amount = 0

        topObject = Top(self.c.get("pageID"), amount)
        topPosts = topObject.getTop()
        topObject.toFile(topPosts, False)

        amount = len(topPosts)
        index = amount
        for post in topPosts:
            message = "{}.\t".format(index) + str(post)
            self.c.newMessage(message, index=False)
            index -= 1
        message = "Today's top " + str(amount) + " confessions are:"
        self.c.newMessage(message, index=False)


    def __str__(self):
        return "Posts the most liked confessions/comments"