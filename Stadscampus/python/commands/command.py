class Command():

    def __init__(self, c, *names, args=list()):
        self.c = c
        self.names = names
        self.args = args

    def check(self, text, args):
        if text in self.names:
            self(args)
            return True
        return False

    def __call__(self, args=None):
        print("No commands linked to", self.names[0])

    def __str__(self):
        return "The commands", self.names[0], "has no representation"
