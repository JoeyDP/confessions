class Users():
    def __init__(self):
        self.users = dict()


    def checkUser(self, alias, pw):
        if alias is not None:
            ret = self.users.get(alias.lower(), "")
            if ret == "":
                if pw is not None:
                    print("Adding user:", alias, "with pw:", pw)
                    self.users.update({alias.lower(): pw})
                return True
            else:
                return pw == ret
        else:
            return True