class Blacklist():
    def __init__(self):
        self.softbl = list()
        self.hardbl = list()
        self.checkbl = list()


    def getSoftBl(self):
        return self.softbl

    def getHardBl(self):
        return self.hardbl

    def getCheckBl(self):
        return self.checkbl

    def censure(self, message):
        lower = message.lower()
        for word in self.hardbl:
            if word in lower:
                print("Found", word, "in post, ignoring the confession.")
                raise AssertionError
        for word in self.softbl:
            if word in lower:
                print("Found", word, "in post, censuring it out.")
                message = message.replace(word, "[censured]")
        return message