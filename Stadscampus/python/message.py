import re
import requests

class Message():
    def __init__(self, c, text, row, image="", index=True):
        self.c = c
        self.row = row
        self.image = image
        self.index = index;
        if len(text) > 1:
            match = re.search(self.c.regex, text)
            if match is not None:
                match = match.groupdict()
            else:
                print(text, "is not a valid confession!")
                return
            self.message = match.get("msg", None)
            self.message = self.c.blacklist.censure(self.message)

            self.msgfor = match.get("to", None)

            alias = match.get("from", None)
            pw = match.get("pw","")
            if not self.c.users.checkUser(alias, pw):
                print("Invalid alias/pw combination for alias:", alias, "pw: ", pw)
        else:
            self.message = text
            self.msgfor = None
            alias = None

        if alias is not None:
            self.alias = "[{0}]".format(alias)
        else:
            self.alias = None

        self.link = self.getLink()


    def __str__(self):
        if self.alias is None:
            ret = "Message from [Anonymous]"
        else:
            ret = "Message from " + self.alias
        if self.msgfor is not None:
            ret += " for " + self.msgfor
        ret += "\n" + self.message
        if self.link is not None:
            ret += "\n link: " + self.link
        return ret.encode("ascii", "ignore").decode("ascii")


    def getFbFormat(self, index):
        ret = ""
        if self.index and self.msgfor is None:
            ret += "#" + str(index) + " "
        if self.alias is not None:
            ret += self.alias + " "
        ret += self.message
        return ret


    def getLink(self):
        match = re.findall(r"(?:http|www.)+s?(?::\/\/)?(?:www.)?[^\s]*(?:\b)?", self.message)
        if len(match) > 0:
            return match[0]
        else:
            return None


    def getPath(self):
        if self.msgfor is not None:
            if int(self.c.get("currIndex")) - int(self.msgfor) <= 100 and int(self.c.get("currIndex")) - int(self.msgfor) > 0:
                req = self.c.pageGraph.get("me/feed")
                feed = req["data"]
                for i in range(4):
                    if feed is not None:
                        for post in feed:
                            try:
                                text = post["message"]
                                if text.startswith("#" + str(self.msgfor) + " "):
                                    id = post["id"]
                                    return "{0}/comments".format(id)
                            except KeyError:
                                pass
                        req = requests.get(req["paging"]["next"]).json()
                        feed = req["data"]
            self.msgfor = None
        if self.image == "":
            return "{0}/feed".format(str(self.c.get("pageID")))
        else:
            return "{0}/photos".format(str(self.c.get("pageID")))


