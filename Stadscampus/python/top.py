import requests
import re
import webbrowser
import time

graph = "https://graph.facebook.com/v2.3/"

class Top():

    def __init__(self, pageId="", amount=0):
        # The credentials of the Confessions App
        self.id = "1381624515471857"
        self.secret = "d773d546228dafc2110f34a91a7b40b9"

        if pageId == "":
            print("Welcome to the top posts tracker!")
            print("This program searches through a page and looks for the most liked posts.")
            print("Stop the algorithm at anytime by pressing ctrl + c and get the top.")
            print("When the top is ready the posts will be opened in your default browser.\n")
        
        self.appToken = self.getToken()
        self.pageID, self.pageName = self.requestPageID(pageId)
        if amount == 0:
            self.amount = self.requestAmount()
        else:
            self.amount = amount
        self.minDate = self.requestDate()

        
    def getToken(self):
        payload = {'grant_type':'client_credentials', 'client_id':self.id, 'client_secret':self.secret}
        file = requests.post(graph + "oauth/access_token?", params=payload)
        result = file.json().get("access_token")
        return result
        
    def requestPageID(self, pageId):
        if pageId != "":
            name = pageId
        else:
            print("What page do you want to scan?")
            name = input()
        
        page = requests.get(graph + "{0}?access_token={1}".format(name.replace(' ', ''), self.appToken)).json()
        # print(page)
        if not "error" in page:
            if pageId != "":
                return page["id"], page["name"]

            try:
                print("name:", page["name"])
                print("id:", page["id"])
                print("description:", page["about"])
                print("category:", page["category"])
            except KeyError:
                pass
            
            print("\nIs this the page you meant? (Y/n)")
            b = input()
            if b == "" or b.lower() == "y":
                return page["id"], page["name"]
            else:
                return self.requestPageID()
        else:
            print("Page not found")
            return self.requestPageID()
        
    def requestDate(self):
        print("Check until what date?")
        print("0 for page creation")
        print("dd/mm/yyyy for a specific date")
        resp = input()
        if resp == "0":
            return True
        else:
            try:
                return Date(resp)
            except ValueError:
                print("Invalid syntax!\n")
                return self.requestDate()
            

    def requestAmount(self):
        print("Top how many?")
        try:
            amount = int(input())
            if amount > 0:
                return amount
            else:
                raise ValueError
        except ValueError|AttributeError:
            print(amount, "is not a valid number")
            return self.requestAmount()
        
    def checkDate(self, date):
        if self.minDate == True:
            return True
        else:
            return self.minDate <= date
        

    def toFile(self, top, printALso=True):
        with open(str(self.pageName) + " " + time.asctime().replace(":", ".") + ".txt", "w") as file:
            print("The top", self.amount, "posts are:")
            for index, post in enumerate(top):
                text = "{}.\t".format(index+1) + str(post)
                file.write(text)
                if printALso:
                    print(text)



    def openBrowser(self, top):
        for post in top:
            webbrowser.open(post.link)



    def getTop(self, top=list(), feed=None, amount=0):
        if feed is None:
            print("Scanning through page...\n")
            link = graph + "{0}/feed?access_token={1}".format(self.pageID, self.appToken)
            req = requests.get(link).json()
            feed = req["data"]
        try:
            while feed is not None:
                for post in feed:
                    post = Post(post, self.appToken)
                    if self.checkDate(post.date):
                        print(post)
                        self.checkPost(post, top)
                    else:
                        return top
                if "paging" in req:
                    print("fetching next page\n")
                    req = requests.get(req["paging"]["next"]).json()
                else:
                    return top
                if "data" in req:
                    feed = req["data"]
                else:
                    feed = None

            top.sort(reverse=True)
            return top
        except KeyboardInterrupt:
            top.sort(reverse=True)
            return top
        except Exception as e:
            print(e)
            if amount < 50:
                return self.getTop(top, feed, amount+1)
            else:
                return top
        

    def checkPost(self, post, top):
        if len(top) < self.amount:
            top.append(post)
        else:
            if not post in top:
                top.sort()
                if top[0] < post:
                    del top[0]
                    top.append(post)

        
class Post():
    def __init__(self, post, token):
        self.date = Date(post["created_time"])
        self.id = post["id"]
        self.link = "https://www.facebook.com/{0}".format(self.id.replace("_", "/posts/"))
        link = graph + "{0}/likes?summary=true&access_token={1}".format(self.id, token)
        r = requests.get(link).json()
        self.likes = r["summary"]["total_count"]
    def __lt__(self, other):
        return self.likes < other.likes
        
    def __str__(self):
        return str(self.date) + "  " + str(self.link) + "  likes: " + str(self.likes) + "\n"
            
            
class Date():
    def __init__(self, text):
        values = re.match(r"^(?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d\d\d\d)", text)
        if values is None:
            values = re.match(r"(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)", text)
        values = values.groupdict()
        if len(values) != 3:
            raise ValueError
        self.day = values["day"]
        self.month = values["month"]
        self.year = values["year"]
    
    
    def __le__(self, other):
        if self.year > other.year:
            return False
        elif self.month > other.month:
            return False
        elif self.day > other.day:
            return False
        return True
        
    def __str__(self):
        return "{}/{}/{}".format(self.day, self.month, self.year)

if __name__ == "__main__":
    top = Top()
    topPosts = top.getTop()
    top.toFile(topPosts)
    # top.openBrowser(topPosts)

