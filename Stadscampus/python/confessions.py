import os
import sys
try:
    from facepy import GraphAPI
    import facepy
    import requests
    import gspread
    import gspread.httpsession
    import http.client
    from oauth2client.client import SignedJwtAssertionCredentials
except ImportError:
    print("Downlaod required modules? (Y/n)")
    inp = str(input())
    if "n" in inp:
        print("Can't run this program without those, quitting..")
        sys.exit()
    else:
        os.system("pip3 install -r modules.txt")
        print("Installation successfull!\n\n")
        from facepy import GraphAPI
        import facepy
        import requests
        import gspread
        import gspread.httpsession
        import http.client
        from oauth2client.client import SignedJwtAssertionCredentials
import time
import threading
import webbrowser
import atexit
import pickle
from collections import deque
import users
import blacklist
import commands.commandHandler
import json

from message import Message


# TODO:
# re in bl
# blacklist check

# re for links
# (http|www.)+s?(:\/\/)?(www.)?[^\s]*(\b)?

# re for names and numbers
# ^(?:(?:\s*(?:#|@|@#|#@)\s*(?P<to>\d+))|(?:\s*\[\s*(?P<from>[^:]+?)\s*(?::\s*(?P<pw>.+?)\s*)?\])){0,2}\s*(?P<msg>.*)\n?
# messages to ppl -> accounts list


class Confessions():

    def __init__(self):
        self.id = "1381624515471857"
        self.secret = "d773d546228dafc2110f34a91a7b40b9"
        self.uri = "https://apps.facebook.com/ua-confessions"
        self.regex = r"^(?:(?:\s*(?:#|@|@#|#@)\s*(?P<to>\d+))|(?:\s*(?:\[|\()\s*(?P<from>[^:]+?)\s*(?::\s*(?P<pw>.+?)\s*)?(?:\]|\)))){0,2}\s*(?P<msg>(?:.|\n)+)"
        self.running = True

        self.users = None
        self.blacklist = None

        self.post = 0 # 0 is idle, 1 is post, 2 is ignore
        self.autoPost = False
        self.extendedOutput = False
        self.requestedPosts = deque() # -> queue
        self.delay = 3

        # commands
        self.cHandler = commands.commandHandler.CommandHandler(self)

        self.settings = dict()
        self.savedSettingNames = ["userToken", "pageID", "currIndex", "currRow", "username", "password", "filename", "device", "smsMail", "smsPass", "lastID", "postImages", "lastImage"]
        self.otherSettingsNames = ["autopost", "extendedOutput", "delay", "users"]

        self.pageGraph = None
        self.pageToken = ""
        self.sheet = None

        atexit.register(self.cHandler.save)

        self.loadSettings()
        self.setup()
        self.thread = threading.Thread(target=self.mainloop)
        self.thread.start()

        self.cHandler.mainloop()


    def getIntInput(self, msg="", max=sys.maxsize, min=-sys.maxsize-1):
        """ Loops until it gets valid input. Prints a space after msg """
        try:
            inp = input(msg + " ")
            ret = int(inp)
        except ValueError:
            print(inp, "is not an integer, try again")
            return self.getIntInput(msg, max, min)
        if ret <= max and ret >= min:
            return ret
        else:
            print(ret, "is not a valid option")
            return self.getIntInput(msg, max, min)


    def loadSettings(self):
        if os.path.exists("settings.dat"):
            with open("settings.dat", "rb") as file:
                self.settings = pickle.Unpickler(file).load()
        if os.path.exists("users.dat"):
            with open("users.dat", "rb") as file:
                self.users = pickle.Unpickler(file).load()
        else:
            self.users = users.Users()
        if os.path.exists("blacklist.dat"):
            with open("blacklist.dat", "rb") as file:
                self.blacklist = pickle.Unpickler(file).load()
        else:
            self.blacklist = blacklist.Blacklist()

    def get(self, name):
        return self.settings.get(name, "")

    def set(self, key, value):
        self.settings.update({key: value})

    def setup(self):
        try:
            userGraph = GraphAPI(self.get("userToken"), version="2.3")
            pages = userGraph.get("me/accounts")["data"]
        except facepy.OAuthError:
            print("You need to validate this app.")
            self.getUserToken()
            self.setup()
            return

        if self.get("pageID") == "":
            print("These are your pages:")
            i = 0
            for page in pages:
                i += 1
                print("[{0}]\t".format(i) + page["name"])
            print("To which page do you want to autopost? (ex.: 1)")
            selection = self.getIntInput(max=i, min=1) - 1
            pageid = pages[selection]["id"]
            self.set("pageID", pageid)
        for page in pages:
            if page["id"] == self.get("pageID"):
                self.pageToken = page["access_token"]
                break

        self.pageGraph = GraphAPI(self.pageToken, version="2.3")
        feed = self.pageGraph.get("me/feed")["data"]
        self.set("currIndex", 0)
        for message in feed:
            try:
                text = message["message"]
                if text.startswith("#"):
                    index = text.split(" ")[0][1:]
                    self.set("currIndex", int(index) + 1)
                    break
            except KeyError:
                pass
                #print(message, "did not contain a message")
            except ValueError:
                pass

        self.loginGoogle()


    def loginGoogle(self):
        try:
            # if self.get("username") == "":
            #     print("Please log in with you google drive account:")
            #     username = input("e-mail: ")
            #     password = input("Password: ")
            #     self.set("username", username)
            #     self.set("password", password)
            json_key = json.load(open('Confessions_gspread.json'))
            scope = ['https://spreadsheets.google.com/feeds']

            credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode('ascii'), scope)
            client = gspread.authorize(credentials)
        except gspread.AuthenticationError:
            print("Incorrect credentials!")
            # return self.loginGoogle()


        self.sheet = self.getSheet(client)
        if self.get("currRow") == "":
            print("At what row do you want to start?")
            inp = self.getIntInput(min=1) 
            self.set("currRow", inp)
        checkCell = self.sheet.cell(self.get("currRow"), 20)
        while checkCell.value != "":
            self.set("currRow", int(self.get("currRow"))+1)
            checkCell = self.sheet.cell(self.get("currRow"), 20)

    def getSheet(self, client):
        try:
            if self.get("filename") == "":
                print("What is the path to the google spreadsheet?")
                name = input()
                sheet = client.open(name).get_worksheet(0)
                self.set("filename", name)
            else:
                sheet = client.open(self.get("filename")).get_worksheet(0)
            return sheet
        except gspread.SpreadsheetNotFound:
            print("File not found, try again.")
            return self.getSheet(client)


    def getMessagesUrl(self):
        return self.getBaseUrl("messages")


    def getMessageUrl(self, messageID):
        return self.getBaseUrl("messages/view/" + str(messageID))


    def getBaseUrl(self, urlType):
        return "http://smsgateway.me/api/v3/" + urlType + "?email=" + self.get("smsMail") + "&password=" + self.get("smsPass")


    def mainloop(self):
        messageFound = False
        while self.running:
            try:
                if not messageFound:
                    time.sleep(int(self.delay))
                messageFound = False
                nextCellCheck = self.sheet.cell(self.get("currRow"), 20)
                nextCellValue = self.sheet.cell(self.get("currRow"), 2)
                if nextCellCheck.value == "":
                    if nextCellValue.value != "":
                        message = str(nextCellValue.value)
                        if len(message) > 5:
                            messageFound = True
                            self.newMessage(message, self.get("currRow"))
                        else:
                            print("Found message:", message, "\nbut it is too short to post.")
                        self.set("currRow", int(self.get("currRow"))+1)
                    else:
                        if self.extendedOutput:
                            print("Checking row:", self.get("currRow"))
                else:
                    self.set("currRow", int(self.get("currRow"))+1)

                if self.get("smsMail") is not "":
                    if self.extendedOutput:
                        print("Checking for texts")

                    text = self.getNewText()
                    if text != 0:
                        # contact = text.get("contact")
                        if text.get("device_id") == self.get("device"):
                            messageFound = True
                            message = text.get("message")
                            self.newMessage(message, -1)

                if self.get("postImages") == "True":
                    images = self.getImages()
                    if images is not None:
                        for image in images:
                            messageFound = True
                            self.newMessage("", -1, image)

                self.handleRequests()

            except requests.HTTPError as e:
                print("Error occured")
                print(e.message)
            except gspread.RequestError as e:
                print("Error occured in gspread module")
                if self.extendedOutput:
                    print(e)
                if str(e).startswith("401"):
                    print("login in again")
                    self.loginGoogle()
            except http.client.CannotSendRequest:
                print("Connection error.. (request failed) trying to log in to google again.")
                self.loginGoogle()
            except http.client.BadStatusLine:
                print("Connection error.. (bad status line) trying to resume")
            except http.client.ResponseNotReady:
                print("Connection error.. (response not ready) trying to resume")


    ''' Returns a new text if one got found, 0 otherwise '''
    def getNewText(self):
        url = self.getMessagesUrl()
        package = requests.get(url).json()
        texts = package.get("result")
        prev = 0
        prevID = 0
        for text in texts:
            id = text.get("id")
            if int(id) == int(self.get("lastID")):
                if prevID != 0:
                    self.set("lastID", prevID)
                return prev
            prev = text
            prevID = id
        return 0


    def getImages(self):
        ret = set()
        firstID = 0

        package = self.pageGraph.get("me/conversations")
        conversations = package["data"]
        for conversation in conversations:
            messages = conversation["messages"]["data"]
            for message in messages:
                if "attachments" in message:
                    attachment = message["attachments"]["data"][0]
                    if firstID == 0:
                        firstID = attachment["id"]
                    if self.get("lastImage") == attachment["id"]:
                        self.set("lastImage", firstID)
                        return ret
                    else:
                        if "image_data" in attachment:
                            ret.add(attachment["image_data"]["url"])

        self.set("lastImage", firstID)
        return ret


    def newMessage(self, text, row=-1, image="", index=True):
        try:
            message = Message(self, text, row, image, index)
        except AssertionError:
            print("The message is discarded")
            return
        if self.autoPost:
            timestamp = time.asctime()
            print("[{0}]".format(timestamp), "Posting:\n", message)
            if message.image != "":
                print(message.image)
            self.fbPost(message)
        else:
            self.requestedPosts.append(message)


    def handleRequests(self):
        while len(self.requestedPosts) > 0:
            timestamp = time.asctime()
            message = self.requestedPosts.popleft()
            if message.row == -1:
                print("[{0}]".format(timestamp), "New text/image message:")
            else:
                print("[{0}]".format(timestamp), "New form message:")
            print(message)
            if message.image != "":
                print(message.image)
            print("[enter] to post, \"no\" to ignore")
            while self.post == 0 and self.running:
                time.sleep(0.1)
                if self.post == 2:
                    print("Cancelled post")
                elif self.post == 1:
                    print("Posting message")
                    self.fbPost(message)
            self.post = 0
            if not self.running:
                self.set("currRow", int(self.get("currRow"))-1)


    def fbPost(self, msg):
        self.pageGraph.post(path=msg.getPath(), message=msg.getFbFormat(self.get("currIndex")), link=msg.link, url=msg.image, attachment_url=msg.image)
        if msg.row != -1:
            self.sheet.update_cell(msg.row, 20, "x")
        print()
        if msg.msgfor is None:        # New confession, increment the index
             self.set("currIndex", int(self.get("currIndex")) + 1)


    def getUserToken(self):
        print("In order to post on your behalf on Facebook, the program needs something called a token.")
        print("When your browser opens the log in screen and asks for permissions, please authorize the app.")
        print("After that you will be redirected to a strange page (I suck at html). The program needs the access token that appeared in the url at the top of your browser.")
        print("The url will look something like this: https://apps.facebook.com/ua-confessions/#access_token=[xxx]&expires_in=[a number]")
        print("Where the [xxx] is the part you need to copy.")
        print("Copy it and paste it here, then press enter.")
        print("If you understand all this press enter.")
        input()
        url = "https://graph.facebook.com/v2.3/oauth/authorize?type=user_agent&client_id={0}&redirect_uri={1}&scope=manage_pages,publish_pages,read_page_mailboxes".format(self.id, self.uri)
        print(url)
        webbrowser.open(url)

        print("Acces token?")
        token = input()
        print("\n\n\n")
        url = "https://graph.facebook.com/v2.3/oauth/access_token?grant_type=fb_exchange_token&client_id={0}&client_secret={1}&fb_exchange_token={2}".format(self.id, self.secret, token)
        r = requests.get(url)
        payload = r.json()
        permToken = payload.get("access_token")
        self.set("userToken", permToken)


if __name__ == "__main__":
    c = Confessions()

