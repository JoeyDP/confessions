import commands.command as command

class CommandSet(command.Command):

    def __init__(self, c):
        super().__init__(c, "set", args=["[setting]","[value]"])

    def __call__(self, args=None):
        try:
            if args is not None:
                try:
                    name = args[0]
                    value = args[1]
                except IndexError:
                    print("Suply a name and value!")
                    return

            else:
                print("Suply a name and value!")
                return

            if name in self.c.savedSettingNames:
                self.c.set(name, value)
                print(name, "set to", self.c.get(name))
            elif name in self.c.otherSettingsNames:
                if name == "autopost":
                    b = (value == "True" or value == "true" or value == "t")
                    if b:
                        self.c.post = 1
                    else:
                        self.c.post = 0
                    self.c.autoPost = b
                    print("autopost changed to", self.c.autoPost)
                elif name == "extendedOutput":
                    self.c.extendedOutput = (value == "True" or value == "true" or value == "t")
                    print("extended output ", end="")
                    if self.c.extendedOutput:
                        print("enabled")
                    else:
                        print("disabled")
                elif name == "delay":
                    self.c.delay = int(value)
                    print("delay set to", self.c.delay)
                elif name == "users":
                    print("Can't set users")
            else:
                print(name, "is not a settings!")
        except ValueError:
            print("Invalid value type")
            return

    def __str__(self):
        return "Sets one of following settings to value:\n" + str(self.c.savedSettingNames + self.c.otherSettingsNames)