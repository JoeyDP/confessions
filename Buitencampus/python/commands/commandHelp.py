import commands.command as command

class CommandHelp(command.Command):

    def __init__(self, c):
        super().__init__(c, "help")

    def __call__(self, args=None):
        print("-"*50)
        commands = self.c.cHandler.commands
        for command in commands:
            print("/".join(command.names), *command.args)
            scom = str(command)
            scom = scom.split("\n")
            for line in scom:
                print("  ", line)
        print("-"*50)
        print()


    def __str__(self):
        return "Display this again."