import commands.command as command
import http.client

class CommandPost(command.Command):

    def __init__(self, c):
        super().__init__(c, "post", args=["[row]"])

    def __call__(self, args=None):
        if args != None:
            try:
                row = int(args[0])
            except ValueError:
                print(args[0], "is not a valid row number.")
                return
        else:
            print("Suply a row!")

        try:
            nextCellValue = self.c.sheet.cell(row, 2)
            if nextCellValue.value != "":
                print("Message at row", row)
                message = str(nextCellValue.value)
                if message is not None:
                    self.c.newMessage(message, row)
            else:
                print("No message found on row", row)
        except http.client.CannotSendRequest:
            print("Http error, try again..")

    def __str__(self):
        return "Posts the message at row in the excel sheet (with confirmation)."