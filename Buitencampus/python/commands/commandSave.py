import commands.command as command
import pickle
import os

class CommandSave(command.Command):

    def __init__(self, c):
        super().__init__(c, "save")

    def __call__(self, args=None):
        with open("settings.dat", "wb") as file:
            pickle.Pickler(file).dump(self.c.settings)
        with open("blacklist.dat", "wb") as file:
            pickle.Pickler(file).dump(self.c.blacklist)
        with open("users.dat", "wb") as file:
            pickle.Pickler(file).dump(self.c.users)
        print("Succesfully saved!")


    def __str__(self):
        return "Saves al settings, blacklist and users."