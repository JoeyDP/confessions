Make a confession: Tijdlijn | Info | CONFESSIONS FORM | Likes | ...
OR: http://goo.gl/forms/xCmgjqY3dd
Anonymous chat: http://www.chatzy.com/13273185466066

New feature:
Send a (not so anonymous) personal message to this page with a picture and it will get posted!

e.g.: @6666 [myname:mypass] This is my confession that will appear as a comment to 6666.

To reply to someone else's confession you need to start your confession with either:
@number
#number

To add an alias type [name:pw] or (name:alias)
pw is optional.

If you want something added to the blacklist, just send us a message.

Posts by admin begin with [Admin].

(We are not responsible for what you submit to the form. Please don't use one of your most frequently used passwords.)
